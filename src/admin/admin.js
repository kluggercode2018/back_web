const string = require('../utils/string');
const gmail = require('../utils/gmail')
const prm = require('../utils/parameters');
const serverless = require('serverless-http');
const bodyParser = require('body-parser');
const express = require('express');
const ejs = require('ejs');
const fs = require('fs');
const uuidv5 = require('uuid/v5');
const AWS = require('aws-sdk');
const app = express();


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Enable CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


// environment variables
const SUBSCRIBE_TABLE = process.env.SUBSCRIBE_TABLE;
const IS_OFFLINE = process.env.IS_OFFLINE;




// Try an offline DynamoDB connection
let dynamoDb;
if (IS_OFFLINE === 'true') {
  dynamoDb = new AWS.DynamoDB.DocumentClient({
    region: 'localhost',
    endpoint: 'http://localhost:8000'
  });
} else
  dynamoDb = new AWS.DynamoDB.DocumentClient();



// Ping endpoint for testing purposes
app.get('/admin/ping', (req, res) => {
  res.json( {message: 'pong'} );

});




/*
Function: http post method
Description:  Insert the email in the database to subscribe in Klugger
Input: email
Output: verified message
*/
app.post('/admin/subscribe',(req, res, next)=> {
  let parameters = req.body
  let parameterAttributes = {
    email : {required:true, type: 'string'}
  }


  let errorMessage =  prm.checkParameters(parameters, parameterAttributes)
  if(errorMessage) {
    console.log(errorMessage)
    res.status(400).json(errorMessage)
  }

  let paramsString = JSON.stringify(parameters.email)
  let subscribeId = uuidv5(paramsString, '1b671a64-40d5-491e-99b0-da01ff1f3341')
  parameters.subscribeId = subscribeId


  let paramsUser = {
    TableName : SUBSCRIBE_TABLE,
    Key:{
      "subscribeId": parameters.subscribeId,
      "email": parameters.email
    }
  }




  let landing = fs.readFileSync(__dirname + '/email/template_landing.html',{encoding:'utf-8'})



  const mailOptions = {
    from:  gmail.user,
    to: parameters.email,
    subject: gmail.subjectKlugger,
    html: ejs.render(landing)
  }

  gmail.transporter.sendMail(mailOptions, function (err, info) {
    if(err){
        console.log(err)
        return res.status(500).json({error:err})
    }else{



        let params = {
          TableName: SUBSCRIBE_TABLE,
          Item: parameters
        }
        // callback nested
        dynamoDb.put(params, (error, data) => {
            if (err) {
                console.log(err);
                return res.status(500).json({ error})
            }else{
                return res.json({message: 'email sent'})
            }
        })
    }
 })


});



module.exports.handler = serverless(app);
