const serverless = require('serverless-http');
const express = require('express');
const app = express();
const _ = require('underscore')
const bodyParser = require('body-parser')
const uuidv5 = require('uuid/v5')
const AWS = require('aws-sdk')
const jwt = require('jsonwebtoken')
const gmail = require('../utils/gmail')
const bcrypt = require('bcrypt')
const cors = require('cors');
const ejs = require('ejs');
const fs = require('fs');

//own
const User = require('../models/User')
const prm = require('../utils/parameters')
const {checkToken} = require('../middlewares/auth')



app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
// Enable CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


// environment variables
const USERS_TABLE = process.env.USERS_TABLE
const IS_OFFLINE = process.env.IS_OFFLINE

// database
let dynamoDb
if (IS_OFFLINE === 'true') {
  dynamoDb = new AWS.DynamoDB.DocumentClient({
    region: 'localhost',
    endpoint: 'http://localhost:8000'
  });
} else
  dynamoDb = new AWS.DynamoDB.DocumentClient()




// testing
app.get('/users/ping',(req,res)=>{
  return res.json({message:'pong'})
});





/*
Function: http post method
Description: Add new user in the database and send email to verification
Input: User info
Output: verified message
*/
app.post('/users/signup' ,(req, res) => {
    let parameters = _.pick(req.body,User.allowedParameters)
    let errorMessage =  prm.checkParameters(parameters, User.parameterAttributes)

    if (errorMessage) {
        console.log(errorMessage)
        return res.status(400).json(errorMessage)
    }

    let paramsString = JSON.stringify(parameters.email)
    let userId = uuidv5(paramsString, prm.hashString)
    parameters.userId = userId;


    let params = {
        TableName : USERS_TABLE,
        KeyConditionExpression: "email = :email",
        ExpressionAttributeValues: {
            ":email": parameters.email
        }
    }

    dynamoDb.query(params, (error, result) => {
      if(error){
          return res.status(500).json({error});
      }else if(result.Items.length!==0){
        return res.status(400).json({message:'email already used'});
      }else{


        // hash password
        let hash = bcrypt.hashSync(parameters.password,10);
        parameters.password = hash;
        parameters.emailVerified = false;

        let paramsPut = {
          TableName:USERS_TABLE,
          Item: parameters
        }

        dynamoDb.put(paramsPut,(errPut,data)=>{
          if(errPut){
            console.log(errPut);
            return res.status(500).json({error:errPut});
          }else{


               let landing = fs.readFileSync(__dirname + '/email/verified.html',{encoding:'utf-8'})
               const mailOptions = {
                 from:  gmail.user,
                 to: parameters.email,
                 subject: "Klugger: Verificar cuenta",
                 html: ejs.render(landing,{link:`https://klugger.mx/login/${parameters.userId}`})
               }

               gmail.transporter.sendMail(mailOptions, function (err, info) {
                 if(err){
                     console.log(err)
                     return res.status(500).json({error:err})
                 }else{
                     return res.json({message:'email sent'});
                 }
              })
          }
        });
      }
    })

});





/*
Function: http post method
Description:  Login with email and password
Input: email,password
Output: token for sessions
*/
app.post('/users/signin' ,(req, res) => {
  let parameters = _.pick(req.body,['email','password']);
  let parameterAttributes = {
      email: { required: true, type: 'string' },
      password: { required: true, type: 'string' }
  }

  let errorMessage =  prm.checkParameters(parameters, parameterAttributes);

  if (errorMessage) {
      console.log(errorMessage);
      return res.status(400).json(errorMessage);
  }

  let params = {
      TableName : USERS_TABLE,
      KeyConditionExpression: "email = :email",
      ExpressionAttributeValues: {
          ":email":parameters.email
      }
  };

  dynamoDb.query(params, (error, result) => {
    if (error){
        console.log(error);
        res.status(500).json({error: error});
    }else if(result.Items.length!==0){
        let match = bcrypt.compareSync(parameters.password,result.Items[0].password);
        if(match){

          if(result.Items[0].emailVerified===false){
              return res.status(400).json({message:'user does not verified'});
          }
            // crear token
            delete result.Items[0].password;
            let token = jwt.sign({
                user: result.Items[0]
            }, "semilla-para-token-klugger", { expiresIn: process.env.EXPIRE_TOKEN });


            let response = {
                user: result.Items[0],
                token
            }

            return res.json(response);
        }else{
            return res.status(400).json({message: "user or password incorrect"});
        }
    }else
        return res.status(400).json({message: "user or password incorrect"});
  })
});




/*
Function: http get method
Description: Get the user info with the token
Output: all user info
*/
app.get('/users/getInfo',checkToken,(req,res)=>{
  return res.json(req.user);
});






/*
Function: http post method
Description: Verification of a new user
Input: userId
Output: verified message
*/
app.post('/users/verified' ,(req, res) => {
  let parameters = _.pick(req.body,['userId']);
  let parameterAttributes = {
      userId: { required: true, type: 'string' }
  }
  let errorMessage =  prm.checkParameters(parameters, parameterAttributes);

  if (errorMessage) {
      console.log(errorMessage);
      return res.status(400).json(errorMessage);
  }
  let params = {
      TableName : USERS_TABLE,
      KeyConditionExpression: "userId = :userId",
      IndexName: 'userId-index',
      ExpressionAttributeValues: {
          ":userId": parameters.userId
      }
  }

  dynamoDb.query(params, (error, result) => {
    if(error){
        return res.status(500).json({error});
    }else if(result.Items.length===0){
      return res.status(400).json({message:'email does not exist'});
    }else{
      let paramsUpdate = {
        TableName: USERS_TABLE,
        Key: {
          "email": result.Items[0].email,
          "userId": parameters.userId
        },
        UpdateExpression: "set emailVerified = :emailVerified",
        ExpressionAttributeValues: {
          ":emailVerified":true
        }
      }

      dynamoDb.update(paramsUpdate,(err,dataUpdated)=>{
          if(err){
            console.log(err);
            return res.status(500).json({error:err});
          }else{
            let response = {
              message: "user verified",
              email: result.Items[0].email
            }
            return res.json(response);
          }
      });

    }
  })

})
module.exports.handler = serverless(app);
