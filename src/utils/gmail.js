const nodemailer = require('nodemailer');
const credentials = require('./credentials.json')


//https://myaccount.google.com/lesssecureapps
let transporter = nodemailer.createTransport({
 service: 'gmail',
 auth: {
        user: credentials.user,
        pass: credentials.pass
    }
});



module.exports.subjectKlugger = "Klugger";
module.exports.transporter = transporter;
module.exports.user = credentials.user;
