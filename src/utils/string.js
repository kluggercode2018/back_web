


module.exports.formatDate = () => {
    let d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}


module.exports.formatNumber = (number) =>{
  let arr=number.split(".");
  let mod=arr[0].length%3;
  let first="";
  for(let i=0;i<arr[0].length;i++){
    if(i>=mod && ((i-mod)%3)===0 && i!==0)
      first+=","

    first+=arr[0][i];
  }
  if(arr.length>1){
    first+=".";
    first+=arr[1];
  }
  return first;
}
