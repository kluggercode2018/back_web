

// USER

const allowedParameters= ['firstName','lastName','password','typeUser','email','gender','mobile'];
let parameterAttributes = {
      firstName: {required: true, type: 'string'},
      lastName: {required: true, type: 'string'},
      password: {required:true,type:'string'},
      typeUser:{required:false,type:'string'},
      email: { required: true, type: 'string' },
      gender:{required:false,type:'boolean'}, //0 woman, 1 man
      mobile: {required:true, type: 'number'}
};


module.exports = {
  allowedParameters,
  parameterAttributes
}
